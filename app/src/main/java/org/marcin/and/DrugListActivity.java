package org.marcin.and;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.marcin.and.about_drugs.OpisLekowActivity;
import org.marcin.and.adapters.AboutDrugs;
import org.marcin.and.adapters.LekiListaManager;
import org.marcin.and.poradnik.WagaActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class DrugListActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String DRUG_LIST = "DrugList";
    @Inject
    Bus mBus;
    @Inject
    ObliczeniaDawek mObliczeniaDawek;
    Toolbar mToolbar;
    RecyclerView mRecyclerId;

    private List<String> mLista;
    private List<String> mListaDawek;
    private int mWaga;
    //private LinkedHashMap<String, Integer> mapIndex;
    Map<String, Integer> mapIndex;
    private String[] wiek_lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_drug_list);
        setContentView(R.layout.lista_lekow_sidebar);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerId = (RecyclerView) findViewById(R.id.recycler_id);

        MyApplication.mComponent.inject(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        String stringExtra = getIntent().getStringExtra(WybierzWiekFragment.WYBIERZWIEK);
        setActionTitle(stringExtra);

        wiek_lista = getResources().getStringArray(R.array.leki);
        String[] wiek_lista_index = getResources().getStringArray(R.array.leki2);
        Arrays.asList(wiek_lista_index);
        prepareTitleList();
        mObliczeniaDawek.onAttach(this);
        mListaDawek = mObliczeniaDawek.oblicz(mWaga);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        mRecyclerId.setLayoutManager(manager);
        mRecyclerId.setAdapter(new LekiListaManager(mLista, mListaDawek));
        getIndexList(wiek_lista_index);
        displayIndex();
    }
    private void displayIndex() {
        LinearLayout indexLayout = (LinearLayout) findViewById(R.id.side_index);

        TextView textView;
        List<String> indexList = new ArrayList<String>(mapIndex.keySet());
        for (String index : indexList) {
            textView = (TextView) getLayoutInflater().inflate(
                    R.layout.side_index_item, null);
            textView.setText(index);
            textView.setOnClickListener(this);
            indexLayout.addView(textView);
        }
    }
    private void setActionTitle(String waga) {
        switch (waga) {
            case WybierzWiekFragment.NWRDK:
                getSupportActionBar().setTitle(R.string.ponizej1mz);
                mWaga = 3;
                break;
            case WybierzWiekFragment.MIESIAC_1:
                getSupportActionBar().setTitle(R.string.dziecko1mz);
                mWaga = 4;
                break;
            case WybierzWiekFragment.MIESIAC_2:
                getSupportActionBar().setTitle(R.string.dziecko2mz);
                mWaga = 5;
                break;
            case WybierzWiekFragment.MIESIAC_3:
                getSupportActionBar().setTitle(R.string.dziecko3mz);
                mWaga = 6;
                break;
            case WybierzWiekFragment.MIESIAC_4:
                getSupportActionBar().setTitle(R.string.dziecko4mz);
                mWaga = 7;
                break;
            case WybierzWiekFragment.MIESIAC_5:
                getSupportActionBar().setTitle(R.string.dziecko5mz);
                mWaga = 8;
                break;
            case WybierzWiekFragment.MIESIAC_6:
                getSupportActionBar().setTitle(R.string.dziecko6mz);
                mWaga = 8;
                break;
            case WybierzWiekFragment.MIESIAC_7:
                getSupportActionBar().setTitle(R.string.dziecko7mz);
                mWaga = 8;
                break;
            case WybierzWiekFragment.MIESIAC_8:
                getSupportActionBar().setTitle(R.string.dziecko8mz);
                mWaga = 9;
                break;
            case WybierzWiekFragment.MIESIAC_9:
                getSupportActionBar().setTitle(R.string.dziecko9mz);
                mWaga = 9;
                break;
            case WybierzWiekFragment.MIESIAC_10:
                getSupportActionBar().setTitle(R.string.dziecko10mz);
                mWaga = 9;
                break;
            case WybierzWiekFragment.MIESIAC_11:
                getSupportActionBar().setTitle(R.string.dziecko11mz);
                mWaga = 9;
                break;
            case WybierzWiekFragment.MIESIAC_12:
                getSupportActionBar().setTitle(R.string.dziecko12mz);
                mWaga = 10;
                break;
            case WybierzWiekFragment.MIESIAC_18:
                getSupportActionBar().setTitle(R.string.dziecko18mz);
                mWaga = 11;
                break;
            case WybierzWiekFragment.LATA1:
                getSupportActionBar().setTitle(R.string.dziecko1lata);
                mWaga = 10;
                break;
            case WybierzWiekFragment.LATA2:
                getSupportActionBar().setTitle(R.string.dziecko2lata);
                mWaga = 12;
                break;
            case WybierzWiekFragment.LATA3:
                getSupportActionBar().setTitle(R.string.dziecko3lata);
                mWaga = 14;
                break;
            case WybierzWiekFragment.LATA4:
                getSupportActionBar().setTitle(R.string.dziecko4lata);
                mWaga = 16;
                break;
            case WybierzWiekFragment.LATA5:
                getSupportActionBar().setTitle(R.string.dziecko5lata);
                mWaga = 18;
                break;
            case WybierzWiekFragment.LATA6:
                getSupportActionBar().setTitle(R.string.dziecko6lata);
                mWaga = 20;
                break;
            case WybierzWiekFragment.LATA7:
                getSupportActionBar().setTitle(R.string.dziecko7lata);
                mWaga = 22;
                break;
            case WybierzWiekFragment.LATA8:
                getSupportActionBar().setTitle(R.string.dziecko8lata);
                mWaga = 25;
                break;
            case WybierzWiekFragment.LATA9:
                getSupportActionBar().setTitle(R.string.dziecko9lata);
                mWaga = 28;
                break;
            case WybierzWiekFragment.LATA10:
                getSupportActionBar().setTitle(R.string.dziecko10lata);
                mWaga = 33;
                break;
            case WybierzWiekFragment.LATA11:
                getSupportActionBar().setTitle(R.string.dziecko11lata);
                mWaga = 36;
                break;
            case WybierzWiekFragment.LATA12:
                getSupportActionBar().setTitle(R.string.dziecko12lata);
                mWaga = 40;
                break;
            case WybierzWiekFragment.LATA13:
                getSupportActionBar().setTitle(R.string.dziecko13lata);
                mWaga = 41;
                break;
        }
    }
    private void prepareTitleList() {
        mLista = new ArrayList<>();
        for (int i = 0; i < wiek_lista.length; i++) {
            mLista.add(wiek_lista[i]);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }
    @Override
    protected void onStop() {
        super.onStop();
        mObliczeniaDawek.onStop();
    }

    @Subscribe
    public void onClickLek(AboutDrugs aboutDrugs) {
        int rodzajLeku = aboutDrugs.position;
        switch (rodzajLeku) {
            case 0:
                Intent intent0 = new Intent(this, WagaActivity.class);
                intent0.putExtra(DRUG_LIST, "WAGA");
                startActivity(intent0);
                break;
            case 1:
                Intent intent1 = new Intent(this, WagaActivity.class);
                intent1.putExtra(DRUG_LIST, "RR");
                startActivity(intent1);
                break;
            case 2:
                Intent intent2 = new Intent(this, WagaActivity.class);
                intent2.putExtra(DRUG_LIST, "ODDECH");
                startActivity(intent2);
                break;
            case 3:
                Intent intent3 = new Intent(this, WagaActivity.class);
                intent3.putExtra(DRUG_LIST, "TETNO");
                startActivity(intent3);
                break;
            case 4:
                Intent intent4 = new Intent(this, WagaActivity.class);
                intent4.putExtra(DRUG_LIST, "GLUKOZA");
                startActivity(intent4);
                break;
            case 5:
                Intent intent5 = new Intent(this, WagaActivity.class);
                intent5.putExtra(DRUG_LIST, "INTUBACJA");
                startActivity(intent5);
                break;
            case 6:
                Intent intent6 = new Intent(this, WagaActivity.class);
                intent6.putExtra(DRUG_LIST, "LARYN");
                startActivity(intent6);
                break;
            case 7:
                Intent intent7 = new Intent(this, WagaActivity.class);
                intent7.putExtra(DRUG_LIST, "MASKA");
                startActivity(intent7);
                break;
            case 8:
                Intent intent8 = new Intent(this, WagaActivity.class);
                intent8.putExtra(DRUG_LIST, "DEFI");
                startActivity(intent8);
                break;
            case 9:
                Intent intent9 = new Intent(this, WagaActivity.class);
                intent9.putExtra(DRUG_LIST, "KARDIO");
                startActivity(intent9);
                break;
            // TODO: 09.01.18 Adenozynę dodać
            //Adenozyna: 0,2 mg/kg, druga: 0,4 mg/kg, 6 mg w 3 ml , 0,1 ml/kg pierwsza, 0,2 ml/kg druga
            case 10:
                Intent intent = new Intent(this, OpisLekowActivity.class);
                intent.putExtra(DRUG_LIST, "ADENOZYNA");
                startActivity(intent);
                break;
            case 11:
                Intent intent10 = new Intent(this, OpisLekowActivity.class);
                intent10.putExtra(DRUG_LIST, "ADRENALINA");
                startActivity(intent10);
                break;
            case 12:
                Intent intent11 = new Intent(this, OpisLekowActivity.class);
                intent11.putExtra(DRUG_LIST, "ADRENALINA");
                startActivity(intent11);
                break;
            case 13:
                Intent intent13 = new Intent(this, OpisLekowActivity.class);
                intent13.putExtra(DRUG_LIST, "ADRENALINA");
                startActivity(intent13);
                break;
            case 14:
                Intent intent14 = new Intent(this, OpisLekowActivity.class);
                intent14.putExtra(DRUG_LIST, "AMIODARON");
                startActivity(intent14);
                break;
            case 15:
                Intent intent15 = new Intent(this, OpisLekowActivity.class);
                intent15.putExtra(DRUG_LIST, "ATROPINA");
                startActivity(intent15);
                break;
            case 16:
                Intent intent16 = new Intent(this, OpisLekowActivity.class);
                intent16.putExtra(DRUG_LIST, "Deksametazon");
                startActivity(intent16);
                break;
            case 17:
            Intent intent17 = new Intent(this, OpisLekowActivity.class);
            intent17.putExtra(DRUG_LIST, "FENTANYL");
            startActivity(intent17);
            break;
            case 18:
                Intent intent155 = new Intent(this, OpisLekowActivity.class);
                intent155.putExtra(DRUG_LIST, "furosemid");
                startActivity(intent155);
                break;
            case 19:
            Intent intent177 = new Intent(this, OpisLekowActivity.class);
            intent177.putExtra(DRUG_LIST, "GLUKAGON");
            startActivity(intent177);
            break;
            case 20:
            Intent intent18 = new Intent(this, OpisLekowActivity.class);
            intent18.putExtra(DRUG_LIST, "GLUKOZA");
            startActivity(intent18);
            break;
            case 21:
            Intent intent19 = new Intent(this, OpisLekowActivity.class);
            intent19.putExtra(DRUG_LIST, "HCT");
            startActivity(intent19);
            break;
            case 22:
            Intent intent20 = new Intent(this, OpisLekowActivity.class);
            intent20.putExtra(DRUG_LIST, "IBUM");
            startActivity(intent20);
            break;
            case 23:
            Intent intent21 = new Intent(this, OpisLekowActivity.class);
            intent21.putExtra(DRUG_LIST, "KETONAL");
            startActivity(intent21);
            break;
            case 24:
                Intent intent144 = new Intent(this, OpisLekowActivity.class);
                intent144.putExtra(DRUG_LIST, "CLEMASTIN");
                startActivity(intent144);
                break;
            case 25:
                Intent intent1555 = new Intent(this, OpisLekowActivity.class);
                intent1555.putExtra(DRUG_LIST, "CLONAZEPAM");
                startActivity(intent1555);
                break;
            case 26:
            Intent intent22 = new Intent(this, OpisLekowActivity.class);
            intent22.putExtra(DRUG_LIST, "MAGNEZ");
            startActivity(intent22);
            break;
            case 27:
            Intent intent23 = new Intent(this, OpisLekowActivity.class);
            intent23.putExtra(DRUG_LIST, "MIDAZOLAM");
            startActivity(intent23);
            break;
            case 28:
                Intent intent24 = new Intent(this, OpisLekowActivity.class);
                intent24.putExtra(DRUG_LIST, "MF");
                startActivity(intent24);
                break;
            case 29:
                Intent intent25 = new Intent(this, OpisLekowActivity.class);
                intent25.putExtra(DRUG_LIST, "BICARBONAT");
                startActivity(intent25);
                break;
            case 30:
                Intent intent26 = new Intent(this, OpisLekowActivity.class);
                intent26.putExtra(DRUG_LIST, "NALOKSON");
                startActivity(intent26);
                break;
            case 31:
                Intent intent27 = new Intent(this, OpisLekowActivity.class);
                intent27.putExtra(DRUG_LIST, "PARA");
                startActivity(intent27);
                break;
            case 32:
                Intent intent28 = new Intent(this, OpisLekowActivity.class);
                intent28.putExtra(DRUG_LIST, "PARA");
                startActivity(intent28);
                break;
            case 33:
                Intent intent29 = new Intent(this, OpisLekowActivity.class);
                intent29.putExtra(DRUG_LIST, "DIAZEPAM");
                startActivity(intent29);
                break;
            case 34:
                Intent intent30 = new Intent(this, OpisLekowActivity.class);
                intent30.putExtra(DRUG_LIST, "DIAZEPAM");
                startActivity(intent30);
                break;
            case 35:
                Intent intent31 = new Intent(this, OpisLekowActivity.class);
                intent31.putExtra(DRUG_LIST, "SALBUTAMOL");
                startActivity(intent31);
                break;
            case 36:
                Intent intent32 = new Intent(this, OpisLekowActivity.class);
                intent32.putExtra(DRUG_LIST, "NACL");
                startActivity(intent32);
                break;
        }
    }

    private void getIndexList(String[] lista) {
        mapIndex = new LinkedHashMap<String, Integer>();
        for (int i = 0; i < lista.length; i++) {
            String elementListy = lista[i];
            String index = elementListy.substring(0, 1);

            if (mapIndex.get(index) == null)
                mapIndex.put(index, i);
        }
    }

    @Override
    public void onClick(View view) {
        TextView selectedIndex = (TextView) view;
        mRecyclerId.getLayoutManager().scrollToPosition(mapIndex.get(selectedIndex.getText()));
        selectedIndex.setTextColor(Color.RED);
    }
}
