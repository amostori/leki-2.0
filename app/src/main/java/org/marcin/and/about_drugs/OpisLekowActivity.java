package org.marcin.and.about_drugs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import org.marcin.and.DrugListActivity;
import org.marcin.and.R;
import org.marcin.and.adapters.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpisLekowActivity extends AppCompatActivity {



    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.recycler_id)
    RecyclerView mRecyclerId;
    String dzialanie, dawkowanie, postac, przeciwwskazania, ciaza, ciekawostki;
    private ExpandableListAdapter.Item mDzialanie;
    private ExpandableListAdapter.Item mPostac;
    private ExpandableListAdapter.Item mDawkowanie;
    private ExpandableListAdapter.Item mPrzeciwwskazania;
    private ExpandableListAdapter.Item mWskazania;
    private ExpandableListAdapter.Item mCiaza;
    private ExpandableListAdapter.Item mCiekawostki;
    private List<ExpandableListAdapter.Item> mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opis_lekow_layout);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String stringExtra = getIntent().getStringExtra(DrugListActivity.DRUG_LIST);

        mRecyclerId.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mData = new ArrayList<>();
        /*data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "kdjf"));
        data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "Postać"));
        data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, "Dawkowanie"));
        data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "Przeciwwskazania"));
        data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "Ciąża i karmienie piersią"));
        data.add(new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "Ciekawostki"));*/

        mPostac = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "POSTAĆ LEKU");
        mDawkowanie = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "DAWKOWANIE");
        mDzialanie = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "DZIAŁANIE");
        mWskazania = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "WSKAZANIA");
        mPrzeciwwskazania = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "PRZECIWSKAZANIA");
        mCiaza = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "CIĄŻA, KARMIENIE PIERSIĄ");
        mCiekawostki = new ExpandableListAdapter.Item(ExpandableListAdapter.HEADER, "CIEKAWOSTKI");

        mDzialanie.invisibleChildren = new ArrayList<>();
        mPostac.invisibleChildren = new ArrayList<>();
        mDawkowanie.invisibleChildren = new ArrayList<>();
        mPrzeciwwskazania.invisibleChildren = new ArrayList<>();
        mWskazania.invisibleChildren = new ArrayList<>();
        mCiaza.invisibleChildren = new ArrayList<>();
        mCiekawostki.invisibleChildren = new ArrayList<>();

        setDane(stringExtra);





        mRecyclerId.setAdapter(new ExpandableListAdapter(mData));
    }

    private void setDane(String string) {

        switch (string) {
            case "ADENOZYNA":
                getSupportActionBar().setTitle(R.string.adenozyna_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_adenozyna)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adenozyna_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adenozyna_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adenozyna_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adenozyna_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adenozyna_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adenozyna_ciekaw)));
                addAllData();
                break;
            case "ADRENALINA":
                getSupportActionBar().setTitle(R.string.adrenalina_nazwy);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_adr)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dawk_adr)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dzialanie_adr)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.wsk_adr)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.przeciw_adr)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.adrenalina_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ciekaw_adr)));
                addAllData();
                break;
            case "AMIODARON":
                getSupportActionBar().setTitle(R.string.amiodaron_title);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_amiodaron)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dawkAmi)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dzialanieAmi)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.wskAmi)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.przeciwAmi)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.amiodaron_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ciekawAmi)));
                addAllData();
                break;
            case "ATROPINA":
                getSupportActionBar().setTitle(R.string.atropina_title);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_atropina)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dawkAtr)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dzialanieAtr)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.wskAtr)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.przeciwAtr)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.atropina_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ciekawAtr)));
                addAllData();
                break;
            case "Deksametazon":
                getSupportActionBar().setTitle(R.string.dexaven_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_dexaven)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dexaven_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dexaven_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dexaven_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dexaven_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dexaven_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.dexaven_ciekaw)));
                addAllData();
                break;
            case "CLEMASTIN":
                getSupportActionBar().setTitle(R.string.clemastin_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_clemastin)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clemastin_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clemastin_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clemastin_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clemastin_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clemastin_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clemastin_ciekaw)));
                addAllData();
                break;
            case "CLONAZEPAM":
                getSupportActionBar().setTitle(R.string.clonazepam_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_clonazepam)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clonazepam_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clonazepam_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clonazepam_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clonazepam_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clonazepam_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.clonazepam_ciekaw)));
                addAllData();
                break;
            case "FENTANYL":
                getSupportActionBar().setTitle(R.string.fentanyl_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_fentanyl)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.fentanyl_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.fentanyl_działanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.fentanyl_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.fentanyl_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.fentanyl_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.fentanyl_ciekaw)));
                addAllData();
                break;
            case "furosemid":
                getSupportActionBar().setTitle(R.string.furosemid_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_furosemid)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.furosemid_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.furosemid_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.furosemid_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.furosemid_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.furosemid_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.furosemid_ciekaw)));
                addAllData();
                break;
            case "GLUKAGON":
                getSupportActionBar().setTitle(R.string.glucagon_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_glukagon)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glucagon_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glucagon_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glucagon_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glucagon_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glucagon_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glucagon_ciekaw)));
                addAllData();
                break;
            case "GLUKOZA":
                getSupportActionBar().setTitle(R.string.glukoza20_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_glukoza20)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glukoza20_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glukoza20_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glukoza20_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glukoza20_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glukoza20_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.glukoza20_ciekaw)));
                addAllData();
                break;
            case "HCT":
                getSupportActionBar().setTitle(R.string.hct_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_hct)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.hct_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.hct_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.hct_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.hct_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.hct_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.hct_ciekaw)));
                addAllData();
                break;
            case "IBUM":
                getSupportActionBar().setTitle(R.string.ibum_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_ibum)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ibum_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ibum_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ibum_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ibum_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ibum_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ibum_ciekaw)));
                addAllData();
                break;
            case "KETONAL":
                getSupportActionBar().setTitle(R.string.ketonal_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_ketonal)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ketonal_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ketonal_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ketonal_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ketonal_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ketonal_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.ketonal_ciekaw)));
                addAllData();
                break;
            case "MAGNEZ":
                getSupportActionBar().setTitle(R.string.magnez_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_magnez)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.magnez_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.magnez_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.magnez_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.magnez_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.magnez_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.magnez_ciekaw)));
                addAllData();
                break;
            case "MIDAZOLAM":
                getSupportActionBar().setTitle(R.string.midazolam_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_midazolam)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.midazolam_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.midazolam_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.midazolam_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.midazolam_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.midazolam_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.midazolam_ciekaw)));
                addAllData();
                break;
            case "MF":
                getSupportActionBar().setTitle(R.string.morfina_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_morfina)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.morfina_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.morfina_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.morfina_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.morfina_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.morfina_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.morfina_ciekaw)));
                addAllData();
                break;
            case "BICARBONAT":
                getSupportActionBar().setTitle(R.string.bicarbonat_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_bicarbonat)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_ciekaw)));
                addAllData();
                break;
            case "NALOKSON":
                getSupportActionBar().setTitle(R.string.nalokson_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_nalokson)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nalokson_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nalokson_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nalokson_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nalokson_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nalokson_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nalokson_ciekaw)));
                addAllData();
                break;
            case "PARA":
                getSupportActionBar().setTitle(R.string.paracetamol_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_paracetamol)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.paracetamol_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.paracetamol_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.paracetamol_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.paracetamol_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.paracetamol_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.paracetamol_ciekaw)));
                addAllData();
                break;
            case "DIAZEPAM":
                getSupportActionBar().setTitle(R.string.diazepam_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_diazepam)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.diazepam_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.diazepam_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.diazepam_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.diazepam_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.diazepam_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.diazepam_ciekaw)));
                addAllData();
                break;
            case "SALBUTAMOL":
                getSupportActionBar().setTitle(R.string.salbutamol_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_salbutamol)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.salbutamol_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.salbutamol_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.salbutamol_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.salbutamol_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.salbutamol_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.salbutamol_ciekaw)));
                addAllData();
                break;
            case "NACL":
                getSupportActionBar().setTitle(R.string.nacl_name);
                mPostac.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.postac_nacl)));
                mDawkowanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nacl_dawkowanie)));
                mDzialanie.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nacl_dzialanie)));
                mWskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nacl_wskazania)));
                mPrzeciwwskazania.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nacl_przeciw)));
                mCiaza.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.bicarbonat_ciaza)));
                mCiekawostki.invisibleChildren.add(new ExpandableListAdapter.Item(ExpandableListAdapter.CHILD, getString(R.string.nacl_ciekaw)));
                addAllData();
                break;
        }

    }

    private void addAllData() {
        mData.add(mPostac);
        mData.add(mDawkowanie);
        mData.add(mDzialanie);
        mData.add(mWskazania);
        mData.add(mPrzeciwwskazania);
        mData.add(mCiaza);
        mData.add(mCiekawostki);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
