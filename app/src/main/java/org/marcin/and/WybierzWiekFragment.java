package org.marcin.and;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.marcin.and.adapters.AgeRecyclerManager;
import org.marcin.and.adapters.WiekClass;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by crimson on 13.10.17.
 */

public class WybierzWiekFragment extends Fragment {
    public static final String WYBIERZWIEK = "WYBIERZWIEK";
    public static final String PONIZEJ_1_MZ = "ponizej1mz";
    public static final String NWRDK = "NWRDK";
    public static final String MIESIAC_1 = "miesiac1";
    public static final String MIESIAC_2 = "MIESIAC2";
    public static final String MIESIAC_3 = "miesiac3";
    public static final String MIESIAC_4 = "miesiac4";
    public static final String MIESIAC_5 = "miesiac5";
    public static final String MIESIAC_6 = "miesiac6";
    public static final String MIESIAC_7 = "miesiac7";
    public static final String MIESIAC_8 = "miesiac8";
    public static final String MIESIAC_9 = "miesiac9";
    public static final String MIESIAC_10 = "miesiac10";
    public static final String MIESIAC_11 = "miesiac11";
    public static final String MIESIAC_12 = "miesiac12";
    public static final String MIESIAC_18 = "miesiac18";
    public static final String LATA1 = "lata1";
    public static final String LATA2 = "lata2";
    public static final String LATA3 = "lata3";
    public static final String LATA4 = "lata4";
    public static final String LATA5 = "lata5";
    public static final String LATA6 = "lata6";
    public static final String LATA7 = "lata7";
    public static final String LATA8 = "lata8";
    public static final String LATA9 = "lata9";
    public static final String LATA10 = "lata10";
    public static final String LATA11 = "lata11";
    public static final String LATA12 = "lata12";
    public static final String LATA13 = "lata13";
    @Inject
    Bus mBus;
    View mView;
    DividerItemDecoration mDividerItemDecoration;
    RecyclerView mRecyclerId;
    private List<String> mLista;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.list_wiek_layout, container, false);
        mRecyclerId = mView.findViewById(R.id.recycler_id);
        MyApplication.mComponent.inject(this);
        prepareList();
        Context context = container.getContext();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mRecyclerId.setLayoutManager(linearLayoutManager);
        mDividerItemDecoration = new DividerItemDecoration(mRecyclerId.getContext(), DividerItemDecoration.VERTICAL);
        mRecyclerId.addItemDecoration(mDividerItemDecoration);
        mRecyclerId.setAdapter(new AgeRecyclerManager(mLista));//Ustawianie adaptera!!!!!!!!!!!!

        // mRecyclerId.setHasFixedSize(true);

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    private void prepareList() {
        String[] wiek_lista = getResources().getStringArray(R.array.wiek_array);
        mLista = new ArrayList<>();
        for (int i = 0; i < wiek_lista.length; i++) {
            mLista.add(wiek_lista[i]);
        }
    }


    @Subscribe
    public void onClickWiek(WiekClass wiekClass) {//from AgeRecyclerManager
        int position = wiekClass.mPosition;
        switch (position) {
            case 0:
                //siwzorodek
                Intent intent0 = new Intent(getContext(), SwiezorodekActivity.class);
                startActivity(intent0);
                break;
            case 1://poniżej 1 m.ż.
                Intent intent = new Intent(getContext(), DrugListActivity.class);
                intent.putExtra(WYBIERZWIEK, NWRDK);
                startActivity(intent);
                break;
            case 2:// 1 m.ż.
                Intent intent2 = new Intent(getContext(), DrugListActivity.class);
                intent2.putExtra(WYBIERZWIEK, MIESIAC_1);
                startActivity(intent2);
                break;
            case 3:// 2 m.ż.
                Intent intent3 = new Intent(getContext(), DrugListActivity.class);
                intent3.putExtra(WYBIERZWIEK, MIESIAC_2);
                startActivity(intent3);
                break;
            case 4:
                Intent intent4 = new Intent(getContext(), DrugListActivity.class);
                intent4.putExtra(WYBIERZWIEK, MIESIAC_3);
                startActivity(intent4);
                break;
            case 5:
                Intent intent5 = new Intent(getContext(), DrugListActivity.class);
                intent5.putExtra(WYBIERZWIEK, MIESIAC_4);
                startActivity(intent5);
                break;
            case 6:
                Intent intent6 = new Intent(getContext(), DrugListActivity.class);
                intent6.putExtra(WYBIERZWIEK, MIESIAC_5);
                startActivity(intent6);
                break;
            case 7:
                Intent intent7 = new Intent(getContext(), DrugListActivity.class);
                intent7.putExtra(WYBIERZWIEK, MIESIAC_6);
                startActivity(intent7);
                break;
            case 8:
                Intent intent8 = new Intent(getContext(), DrugListActivity.class);
                intent8.putExtra(WYBIERZWIEK, MIESIAC_7);
                startActivity(intent8);
                break;
            case 9:
                Intent intent9 = new Intent(getContext(), DrugListActivity.class);
                intent9.putExtra(WYBIERZWIEK, MIESIAC_8);
                startActivity(intent9);
                break;
            case 10:
                Intent intent10 = new Intent(getContext(), DrugListActivity.class);
                intent10.putExtra(WYBIERZWIEK, MIESIAC_9);
                startActivity(intent10);
                break;
            case 11:
                Intent intent11 = new Intent(getContext(), DrugListActivity.class);
                intent11.putExtra(WYBIERZWIEK, MIESIAC_10);
                startActivity(intent11);
                break;
            case 12:
                Intent intent12 = new Intent(getContext(), DrugListActivity.class);
                intent12.putExtra(WYBIERZWIEK, MIESIAC_11);
                startActivity(intent12);
                break;
            case 13:
                Intent intent13 = new Intent(getContext(), DrugListActivity.class);
                intent13.putExtra(WYBIERZWIEK, MIESIAC_12);
                startActivity(intent13);
                break;
            case 14:
                Intent intent14 = new Intent(getContext(), DrugListActivity.class);
                intent14.putExtra(WYBIERZWIEK, MIESIAC_18);
                startActivity(intent14);
                break;
            case 15:
                Intent intent15 = new Intent(getContext(), DrugListActivity.class);
                intent15.putExtra(WYBIERZWIEK, LATA1);
                startActivity(intent15);
                break;
            case 16:
                Intent intent16 = new Intent(getContext(), DrugListActivity.class);
                intent16.putExtra(WYBIERZWIEK, LATA2);
                startActivity(intent16);
                break;
            case 17:
                Intent intent17 = new Intent(getContext(), DrugListActivity.class);
                intent17.putExtra(WYBIERZWIEK, LATA3);
                startActivity(intent17);
                break;
            case 18:
                Intent intent18 = new Intent(getContext(), DrugListActivity.class);
                intent18.putExtra(WYBIERZWIEK, LATA4);
                startActivity(intent18);
                break;
            case 19:
                Intent intent19 = new Intent(getContext(), DrugListActivity.class);
                intent19.putExtra(WYBIERZWIEK, LATA5);
                startActivity(intent19);
                break;
            case 20:
                Intent intent20 = new Intent(getContext(), DrugListActivity.class);
                intent20.putExtra(WYBIERZWIEK, LATA6);
                startActivity(intent20);
                break;
            case 21:
                Intent intent21 = new Intent(getContext(), DrugListActivity.class);
                intent21.putExtra(WYBIERZWIEK, LATA7);
                startActivity(intent21);
                break;
            case 22:
                Intent intent22 = new Intent(getContext(), DrugListActivity.class);
                intent22.putExtra(WYBIERZWIEK, LATA8);
                startActivity(intent22);
                break;
            case 23:
                Intent intent23 = new Intent(getContext(), DrugListActivity.class);
                intent23.putExtra(WYBIERZWIEK, LATA9);
                startActivity(intent23);
                break;
            case 24:
                Intent intent24 = new Intent(getContext(), DrugListActivity.class);
                intent24.putExtra(WYBIERZWIEK, LATA10);
                startActivity(intent24);
                break;
            case 25:
                Intent intent25 = new Intent(getContext(), DrugListActivity.class);
                intent25.putExtra(WYBIERZWIEK, LATA11);
                startActivity(intent25);
                break;
            case 26:
                Intent intent26 = new Intent(getContext(), DrugListActivity.class);
                intent26.putExtra(WYBIERZWIEK, LATA12);
                startActivity(intent26);
                break;
            case 27:
                Intent intent27 = new Intent(getContext(), DrugListActivity.class);
                intent27.putExtra(WYBIERZWIEK, LATA13);
                startActivity(intent27);
                break;
        }
    }
}
