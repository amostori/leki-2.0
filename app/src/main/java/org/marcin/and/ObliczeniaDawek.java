package org.marcin.and;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by crimson on 23.10.17.
 */

public class ObliczeniaDawek {
    public List<String> dawki = new ArrayList<>();
    public DrugListActivity mDrugListActivity;
    public int waga;


    public ObliczeniaDawek() {
    }

    public List<String> oblicz(int waga) {
        String wagaWlasciwa = waga + " " + mDrugListActivity.getString(R.string.waga_dawki);
        dawki.add(wagaWlasciwa);

        String cisnienie = setCisnienie(waga);
        dawki.add(cisnienie);

        String oddech = setOddech(waga);
        dawki.add(oddech);

        String tetno = setTetno(waga);
        dawki.add(tetno);

        String glukoza = setGlukoza(waga);
        dawki.add(glukoza);

        String rurka_intubacyjna = setRurka(waga);
        dawki.add(rurka_intubacyjna);

        String laryngoskop = setLaryngoskop(waga);
        dawki.add(laryngoskop);

        String maskaKrtaniowa = setMaska(waga);
        dawki.add(maskaKrtaniowa);

        String defibrylacja = mDrugListActivity.getString(R.string.defibrylacja_dawka1) + " " + (waga * 4) + " J " + mDrugListActivity.getString(R.string.defibrylacja_dawka2);
        dawki.add(defibrylacja);

        String kardiowersja = mDrugListActivity.getString(R.string.kardio_dawki1) + " " + (waga) + " J " + mDrugListActivity.getString(R.string.kardio_dawki2) + " " + (waga * 2) + " J " +
                mDrugListActivity.getString(R.string.defibrylacja_dawka2);

        dawki.add(kardiowersja);
        //Adenozyna: 0,2 mg/kg, druga: 0,4 mg/kg, 6 mg w 3 ml , 0,1 ml/kg pierwsza, 0,2 ml/kg druga

        String adenozyna = setAdenozyna(waga);
        dawki.add(adenozyna);
        String adrenalina = mDrugListActivity.getString(R.string.adrenalina_dawki) + " " + getDawkaRound(waga, 0.1) + " " +
                mDrugListActivity.getString(R.string.adrenalina_dawki2) + " " + getDawkaRound(waga, 0.01) + " " + "mg leku.";
        dawki.add(adrenalina);

        String adrenalinaAnafilaksja = setAdrenalinaAnafilaksja(waga);
        dawki.add(adrenalinaAnafilaksja);

        String adrenalina_we_wlewie = "Rozcieńcz 1 mg w 50 ml 0,9\u0025 NaCl. Podaj w pompie infuzyjnej z szybkością" + " " + getAdrenalinaWeWlewie(waga) + " " +
                "ml/h.\nDawka podana:" + " " + getDawkaRound(Integer.valueOf(getAdrenalinaWeWlewie(waga)), 20) + " " + "\u00B5g/h";
        dawki.add(adrenalina_we_wlewie);

        String amiodaron = setAmiodaron(waga);
        dawki.add(amiodaron);

        //String atropina = mDrugListActivity.getString(R.string.atropina_dawka1) + " " + getDawkaRound(waga, 0.1) + " " + mDrugListActivity.getString(R.string.adrenalina_dawki2);
        String atropina = setAtropina(waga);
        dawki.add(atropina);

        String klemastin = mDrugListActivity.getString(R.string.klemastin_dawka);

        String dexaven = setDexaven(waga);
        dawki.add(dexaven);

        String fentanyl = mDrugListActivity.getString(R.string.fentanyl_dawki1) + " " + getDawkaRound(waga, 0.1) + " " + mDrugListActivity.getString(R.string.midazolam_dawki2);
        dawki.add(fentanyl);

        String furosemid = setFurosemid(waga);
        dawki.add(furosemid);
        //glukagon
        String glukagon = setGlukagon(waga);
        dawki.add(glukagon);
        String glukoza20 = mDrugListActivity.getString(R.string.glukoza20) + " " + waga + " " + mDrugListActivity.getString(R.string.glukoza20a);
        dawki.add(glukoza20);
        String hydrokortyzon = setHydrokortyzon(waga);
        dawki.add(hydrokortyzon);
        String ibum = setIbum(waga);
        dawki.add(ibum);
        //ketonal
        dawki.add(klemastin);//ponieważ również nie podaje się, jak w przypadku klemastinu
        //klemastin
        dawki.add(klemastin);
        String clonazepam = setClonazepam(waga);
        dawki.add(clonazepam);
        String magnez = mDrugListActivity.getString(R.string.magnez_dawki1) + " " + getDawkaRound(waga, 0.2) + " " + mDrugListActivity.getString(R.string.magnez_dawki2);
        dawki.add(magnez);
        String midazolam = mDrugListActivity.getString(R.string.midazolam_dawki1) + " " + getDawkaRound(waga, 0.1) + " " + mDrugListActivity.getString(R.string.midazolam_dawki2);
        dawki.add(midazolam);
        String morfina = mDrugListActivity.getString(R.string.morfina_dawki1) + " " +
                getDawkaRound(waga, 0.1) + " " + mDrugListActivity.getString(R.string.morfina_dawki2);
        dawki.add(morfina);
        String bicarbonat = setBiarbonat(waga);
        dawki.add(bicarbonat);
        String nalokson = setNalokson(waga);
        dawki.add(nalokson);
        String paracetamolCzopki = setParacetamolCzopki(waga);
        dawki.add(paracetamolCzopki);
        String paracetamol = setParacetamolIv(waga);
        dawki.add(paracetamol);
        //salbutamol
        String relanium = mDrugListActivity.getString(R.string.relanium_dawka1) + " " + getDawkaRound(waga, 0.05) + " " + mDrugListActivity.getString(R.string.relanium_dawka2);
        dawki.add(relanium);
        String relsed = setRelsed(waga);
        dawki.add(relsed);
        String salbutamol = setSalbutamol(waga);
        dawki.add(salbutamol);
        String wlew = setWlew(waga);
        dawki.add(wlew);

        return dawki;

    }

    private String setFurosemid(int waga) {
        String furosemid = "";
        if (waga < 21) {
            furosemid = "Stosuj wyjątkowo. \nNabierz 20 mg leku (1 ampułka) do strzykawki dwudziestki i rozcieńcz do 20 ml 0.9\u0025 NaCl. Podaj powoli" + " " + waga + " " +
                    "ml roztworu. Podałeś" + " " + waga + " " + "mg furosemidu.";
        } else {
            furosemid = "Stosuj wyjątkowo. \nPodaj 1 ampułkę (20 mg) iv powoli, w ciągu 5 min. Można rozcieńczyć do 10 ml solą fizjologiczną.";
        }
        return furosemid;
    }

    private String setDexaven(int waga) {
/*
        String dexaven = "";
        0.15 mg/kg 10 kg - 1,5 mg
                3 kg 0.6 = 1,8mg
                4 kg 0.6 = 2.4 mg
                5 kg 0.6 = 3 mg
                6 = 3.6 mg
                7 kg = 4.
                7 kg = 4 m.ż
                10 kg = 0.4 mg/kg = 4 mg
                20 kg = 0.4 mg/kg = 8 mg
        return null;
        0.5 mg 4 mg w ampułce
        4 mg w 4 ml
        1 mg w 1 ml
        0,1 mg w 0.1 ml

*/
        String dexaven = "";
        if (waga < 10) {
            dexaven = "Nabierz 4 mg do strzykawki dziesiątki i rozcieńcz do 10 ml solą fizjologiczną. Podaj" + " " + getDawkaRound(waga, 0.1) + " "
                    + "ml roztworu iv. Podałeś" + " " + getDawkaRound(waga, 0.4) + " " + "mg deksametazonu.";
        } else {
            dexaven = "Podaj 4 mg leku iv lub im. Przed podaniem dożylnym lek rozcieńcz do 10 ml solą fizjologiczną.";
        }
        return dexaven;
    }

    private String setAdenozyna(int waga) {
        String adenozyna = "";
        if (waga < 16) {
            adenozyna = "Nabierz 1 ampułkę ( 6 mg) do strzykawki dziesiątki i rozcieńcz do 3 ml solą fizjologiczną. Podaj" + " " +
                    getDawkaRound(waga, 0.1) + " " + "ml i.v." + " " +
                    "Druga dawka to " + " " + getDawkaRound(waga, 0.2) + " " + "ml i.v.";
        } else if (waga > 15 && waga < 31) {
            adenozyna = "Nabierz 1 ampułkę ( 6 mg) do strzykawki dziesiątki i rozcieńcz do 3 ml solą fizjologiczną. Podaj" + " " +
                    getDawkaRound(waga, 0.1) + " " + "ml i.v." + " " + "Druga dawka: 2 ampułki rozcieńcz do 6 ml i podaj " + " " +
                    getDawkaRound(waga, 0.2) + " " + "ml i.v.";
        } else {

            adenozyna = "Podaj 6 mg (jedna ampułka) w pierwszej dawce i 12 mg w kolejnej. Jeśli nadal brak efektu powtórz 12 mg.";
        }
        return adenozyna;
    }

    private String setAtropina(int waga) {
        String atropina = "";
        if (waga < 6) {
            atropina = mDrugListActivity.getString(R.string.atropina_dawka2) + " " + mDrugListActivity.getString(R.string.ioiv);
        } else {
            atropina = mDrugListActivity.getString(R.string.atropina_dawka1) + " " + getDawkaRound(waga, 0.1) + " " + mDrugListActivity.getString(R.string.adrenalina_dawki2);
        }
        return atropina;
    }

    private String setWlew(int waga) {
        String wlew = "";
        if (waga < 5) {
            wlew = mDrugListActivity.getString(R.string.wlew_dawki1) + " " + getDawkaRound(waga, 10) + " " + mDrugListActivity.getString(R.string.wlew_dawki2);
        } else if (waga > 4 && waga < 28) {
            wlew = mDrugListActivity.getString(R.string.wlew_dawki1) + " " + getDawkaRound(waga, 20) + " " + mDrugListActivity.getString(R.string.wlew_dawki2);
        } else {
            wlew = mDrugListActivity.getString(R.string.wlew_adult);
        }
        return wlew;
    }

    private String setNalokson(int waga) {
        String nalokson = "";
        if (waga < 41) {
            nalokson = mDrugListActivity.getString(R.string.nalokson_dawka1) + " " +
                    getDawkaRound(waga, 0.1) + " " + mDrugListActivity.getString(R.string.nalokson_dawka2);
        } else {
            nalokson = mDrugListActivity.getString(R.string.nalokson_adult);
        }
        return nalokson;
    }

    private String setBiarbonat(int waga) {
        String bicarbon = "";
        if (waga < 13) {
            bicarbon = mDrugListActivity.getString(R.string.bicarbon_dawki1) + " " + waga + " " + mDrugListActivity.getString(R.string.bicarbon_dawki2);

        } else if (waga > 12) {
            bicarbon = mDrugListActivity.getString(R.string.bicarbon_dawki1) + " " + waga + " " + mDrugListActivity.getString(R.string.bicarbon_dawki3);
        }
        return bicarbon;
    }

    private String setIbum(int waga) {
        String ibum = "";
        if (waga < 8) {
            ibum = mDrugListActivity.getString(R.string.ibum_dawki1);
        } else if (waga > 7 && waga < 13) {

            ibum = mDrugListActivity.getString(R.string.ibum_dawki2) + " " + getDawkaRound(waga, 5) + " " + mDrugListActivity.getString(R.string.ibum_dawki3);
        } else if (waga > 12) {
            ibum = mDrugListActivity.getString(R.string.ibum_dawki4) + " " + getDawkaRound(waga, 5) + " " + mDrugListActivity.getString(R.string.ibum_dawki3);
        }
        return ibum;
    }

    private String setParacetamolIv(int waga) {
        String paracetamol = "";
        if (waga < 11) {
            String dawkaRound = getDawkaRound(waga, 0.75);
            double mnoznik = Double.valueOf(getDawkaRound(waga, 0.75));
            paracetamol = "Podaj" + " " + dawkaRound + " " + "ml (" + wyliczMgParacetamolu(mnoznik) + " " + "mg) leku we wlewie w ciągu 15 min. Dawkę rozcieńcz w " +
                    "stosunku nie większym niż 1:10 w 0,9\u0025 NaCl lub 5\u0025 glukozie.";

        } else if (waga > 10 && waga < 33) {
            String dawkaRound = getDawkaRound(waga, 1.5);
            double mnoznik = Double.valueOf(getDawkaRound(waga, 1.5));

            paracetamol = "Podaj" + " " + dawkaRound + " " + "ml (" + wyliczMgParacetamolu(mnoznik) + " " + "mg) leku we wlewie w ciągu 15 min. Dawkę rozcieńcz w " +
                    "stosunku nie większym niż 1:10 w 0,9\u0025 NaCl lub 5\u0025 glukozie.";
        }else if (waga > 32) {
            paracetamol = mDrugListActivity.getString(R.string.paracetamol_iv3);
        }

       /* if (waga < 11) {
            paracetamol = mDrugListActivity.getString(R.string.paracetamol_iv1) + " " + getDawkaRound(waga, 0.75) + " " + mDrugListActivity.getString(R.string.paracetamol_iv2);
        } else if (waga > 10 && waga < 12) {
            paracetamol = mDrugListActivity.getString(R.string.paracetamol_iv1) + " " + getDawkaRound(waga, 1.5) + " " +
                    mDrugListActivity.getString(R.string.paracetamol_iv2);
        } else if (waga > 11 && waga < 33) {
            paracetamol = mDrugListActivity.getString(R.string.paracetamol_iv5) + " " + getDawkaToMinus(getDawkaRound(waga, 1.5)) + " " +
                    mDrugListActivity.getString(R.string.paracetamol_iv6) +
                    getDawkaRound(waga, 1.5) + " " + mDrugListActivity.getString(R.string.paracetamol_iv7);

        } else if (waga > 32) {
            paracetamol = mDrugListActivity.getString(R.string.paracetamol_iv3);
        }*/
        return paracetamol;
    }



    private String setParacetamolCzopki(int waga) {
        String czopekP = "";
        if (waga < 7) {
            czopekP = mDrugListActivity.getString(R.string.czopek1);
        } else if (waga > 6 && waga < 11) {
            czopekP = mDrugListActivity.getString(R.string.czopek2);
        } else if (waga > 10 && waga < 15) {
            czopekP = mDrugListActivity.getString(R.string.czopek3);
        } else if (waga > 14 && waga < 29) {
            czopekP = mDrugListActivity.getString(R.string.czopek4);
        } else {
            czopekP = mDrugListActivity.getString(R.string.czopek5);
        }
        return czopekP;
    }

    private String setGlukagon(int waga) {
        String glukagon = "";
        if (waga < 26) {
            glukagon = mDrugListActivity.getString(R.string.glukagon_dawki1);
        } else {
            glukagon = mDrugListActivity.getString(R.string.glukagon_dawki2);
        }
        return glukagon;
    }

    private String setClonazepam(int waga) {
        String clonazepam = "";
        if (waga < 40) {
            clonazepam = mDrugListActivity.getString(R.string.clonazepam_dawki1);
        } else {
            clonazepam = mDrugListActivity.getString(R.string.clonazepam_dawki1);
        }
        return clonazepam;
    }

    private String setRelsed(int waga) {
        String relsed = "";
        if (waga < 10) {
            relsed = mDrugListActivity.getString(R.string.wlewka_dawki1) + " " + getDawkaRound(waga, 0.5) + " " + mDrugListActivity.getString(R.string.wlewka_dawki4);
        } else if (waga < 16) {
            relsed = mDrugListActivity.getString(R.string.wlewka_dawki2);
        } else {
            relsed = mDrugListActivity.getString(R.string.wlewka_dawki3);

        }
        return relsed;
    }

    private String setHydrokortyzon(int waga) {
        String hct = "";
        if (waga < 9) {
            hct = mDrugListActivity.getString(R.string.hct_dawki1);
        } else if (waga > 8 && waga < 21) {
            hct = mDrugListActivity.getString(R.string.hct_dawki2);
        } else if (waga < 41) {
            hct = mDrugListActivity.getString(R.string.hct_dawki3);
        } else {
            hct = mDrugListActivity.getString(R.string.hct_dawki4);
        }
        return hct;
    }

    private String setSalbutamol(int waga) {
        String salbutamol = "";
        if (waga < 20) {
            salbutamol = mDrugListActivity.getString(R.string.salbutamol_dawka20);
        } else {
            salbutamol = mDrugListActivity.getString(R.string.salbutamol_dawka30);
        }
        return salbutamol;
    }

    private String setAmiodaron(int waga) {
        String amiodaron = "";
        if (waga < 21) {
            amiodaron = mDrugListActivity.getString(R.string.amiodaron_dawka1) + " " + waga + " " + mDrugListActivity.getString(R.string.amiodaron_dawka2) + " " +
                    getDawkaRound(waga, 5) + " " + "ml leku.";

        } else if (waga > 20 && waga < 31) {
            amiodaron = mDrugListActivity.getString(R.string.amiodaron_dawka3) + " " + getDawkaRound(waga, 0.5) + " " + mDrugListActivity.getString(R.string.amiodaron_dawka2) + " " +
                    getDawkaRound(waga, 5) + " " + "ml leku.";

        } else if (waga == 33) {
            amiodaron = mDrugListActivity.getString(R.string.amiodaron_10lat);
        } else if (waga == 36) {
            amiodaron = mDrugListActivity.getString(R.string.amiodaron_11lat);
        } else if (waga == 40) {
            amiodaron = mDrugListActivity.getString(R.string.amiodaron_dawka5);

        } else if (waga > 40) {
            amiodaron = mDrugListActivity.getString(R.string.amiodaron_dawka6);

        }
        return amiodaron;
    }

    private String setAdrenalinaAnafilaksja(int waga) {
        String anafilaksjaAdrenalina = "";
        if (waga < 20) {
            anafilaksjaAdrenalina = mDrugListActivity.getString(R.string.anafilaksja_dawka_0);
        } else if (waga > 19 && waga < 40) {
            anafilaksjaAdrenalina = mDrugListActivity.getString(R.string.anafilaksja_dawka_6);
        } else if (waga > 39) {
            anafilaksjaAdrenalina = mDrugListActivity.getString(R.string.anafilaksja_dawka_12);

        }
        return anafilaksjaAdrenalina;
    }


    private String getDawkaRound(int waga, double mnoznik) {
        String result = "";
        double dawka_adrenaliny = waga * mnoznik;
        dawka_adrenaliny *= 100;
        dawka_adrenaliny = Math.round(dawka_adrenaliny);
        dawka_adrenaliny /= 100;
        if (dawka_adrenaliny % 1 == 0) {
            result = String.format("%.0f", dawka_adrenaliny);
        } else {
            result = String.valueOf(dawka_adrenaliny);

        }
        return result;
    }

    private String wyliczMgParacetamolu(double ml) {
        String dawkaParacetWMg = "";
        double dawka = ml * 10;
        dawka *= 100;
        dawka = Math.round(dawka);
        dawka /= 100;
        if (dawka % 1 == 0) {
            dawkaParacetWMg = String.format("%.0f", dawka);
        } else {
            dawkaParacetWMg = String.valueOf(dawka);
        }
        return dawkaParacetWMg;
    }

    private String getAdrenalinaWeWlewie(int waga) {
        String result = "";

        double szybkosc = waga / 3;
        szybkosc *= 100;
        szybkosc = Math.round(szybkosc);
        szybkosc /= 100;
        if (szybkosc % 1 == 0) {
            result = String.format("%.0f", szybkosc);
        } else {
            result = String.valueOf(szybkosc);
        }
        return result;
    }

    private String getDawkaToMinus(String number) {
        String result = "";
        Double aDouble = Double.valueOf(number);
        double wynik = 50 - aDouble;
        if (wynik % 1 == 0) {
            result = String.format("%.0f", wynik);
        } else {
            result = String.valueOf(wynik);
        }
        return result;
    }

    private String setMaska(int waga) {
        String maska = "";
        if (waga < 5) {
            maska = mDrugListActivity.getString(R.string.maska_dawka_1);
        } else if (waga > 4 && waga < 11) {
            maska = mDrugListActivity.getString(R.string.maska_dawka_15);
        } else if (waga > 10 && waga < 22) {
            maska = mDrugListActivity.getString(R.string.maska_dawka7l);
        } else if (waga > 21 & waga < 29) {
            maska = mDrugListActivity.getString(R.string.maska_dawka9l);
        } else if (waga > 28) {
            maska = mDrugListActivity.getString(R.string.maska_d);
        }
        return maska;
    }

    private String setLaryngoskop(int waga) {
        String laryngoskop = "";
        if (waga == 3) {
            laryngoskop = mDrugListActivity.getString(R.string.laryng_dawki_nwdk);
        } else if (waga > 3 && waga < 13) {
            laryngoskop = mDrugListActivity.getString(R.string.laryng_dawki_2l);
        } else if (waga > 12 && waga < 19) {
            laryngoskop = mDrugListActivity.getString(R.string.laryng_dawki_5l);

        } else if (waga > 19 && waga < 41) {
            laryngoskop = mDrugListActivity.getString(R.string.laryng_dawki_10l);

        } else if (waga > 40) {
            laryngoskop = mDrugListActivity.getString(R.string.laryng_dawki_12l);
        }
        return laryngoskop;
    }

    private String setRurka(int waga) {
        String rurka = "";
        if (waga == 3) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_nwdk);
        } else if (waga > 3 && waga < 9) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_6mz);
        } else if (waga > 8 && waga < 11) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_1r);
        } else if (waga > 10 && waga < 13) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_2l);
        } else if (waga > 12 && waga < 17) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_4l);
        } else if (waga > 16 && waga < 21) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_6l);
        } else if (waga > 21 && waga < 26) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_8l);
        } else if (waga == 28) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_9l);
        } else if (waga > 28 && waga < 40) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_11l);
        } else if (waga > 39) {
            rurka = mDrugListActivity.getString(R.string.rurka_dawka_12l);
        }
        return rurka;
    }

    private String setGlukoza(int waga) {
        String glukoza = "";
        if (waga == 3) {
            glukoza = mDrugListActivity.getString(R.string.glukoza_dawka_nwdk);
        } else if (waga > 3 && waga < 11) {
            glukoza = mDrugListActivity.getString(R.string.glukoza_dawka_pszk);

        } else if (waga > 10) {
            glukoza = mDrugListActivity.getString(R.string.glukoza_dawka_szk);
        }
        return glukoza;
    }

    @NonNull
    private String setOddech(int waga) {
        String oddech = "";
        if (waga == 3) {
            oddech = mDrugListActivity.getString(R.string.oddech_nwdk);
        } else if (waga > 3 && waga < 13) {
            oddech = mDrugListActivity.getString(R.string.oddech_1mz);

        } else if (waga > 12 && waga < 33) {
            oddech = mDrugListActivity.getString(R.string.oddech_pszk);
        } else if (waga > 28 && waga < 40) {
            oddech = mDrugListActivity.getString(R.string.oddech_szk);
        } else {
            oddech = mDrugListActivity.getString(R.string.oddech_dorosly);
        }
        return oddech;
    }

    private String setTetno(int waga) {
        String tetno = "";

        if (waga < 7) {
            tetno = mDrugListActivity.getString(R.string.tetno_dawka_nmwl);
        } else if (waga > 6 && waga < 13) {
            tetno = mDrugListActivity.getString(R.string.tetno_dawka_pdr);
        } else if (waga > 12 && waga < 34) {
            tetno = mDrugListActivity.getString(R.string.tetno_dawka_pszk);
        } else if (waga > 33) {
            tetno = mDrugListActivity.getString(R.string.tetno_dawka_szk);
        }
        return tetno;
    }

    @NonNull
    private String setCisnienie(int waga) {
        String cisnienie = "";
        if (waga == 3) {
            cisnienie = mDrugListActivity.getString(R.string.cisnienie_dawki1);
        } else if (waga > 3 && waga < 12) {
            cisnienie = mDrugListActivity.getString(R.string.cisnieni_dawki2);
        } else if (waga > 11 && waga < 14) {
            cisnienie = mDrugListActivity.getString(R.string.cisnieni_dawki3);
        } else if (waga > 13 && waga < 20) {
            cisnienie = mDrugListActivity.getString(R.string.cisnieni_dawki4);
        } else if (waga > 19 && waga < 25) {
            cisnienie = mDrugListActivity.getString(R.string.cisnieni_dawki5);
        } else if (waga > 24 && waga < 40) {
            cisnienie = mDrugListActivity.getString(R.string.cisnieni_dawki6);
        } else if (waga > 39) {
            cisnienie = mDrugListActivity.getString(R.string.cisnieni_dawki7);
        }
        return cisnienie;
    }




    public void onAttach(DrugListActivity mDrugListActivity) {
        this.mDrugListActivity = mDrugListActivity;
    }

    public void onStop() {
        mDrugListActivity = null;
    }
}
