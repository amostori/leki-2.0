package org.marcin.and;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;

import org.marcin.and.adapters.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar mToolbar;
    NavigationView mNavigationView;
    DrawerLayout mDrawerLayout;
    TabLayout mTabLayoutId;
    ViewPager mViewPagerId;
    private ChangeLog cl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen_layout);
        cl = new ChangeLog(this);
        if (cl.firstRun())
            cl.getLogDialog().show();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mTabLayoutId = (TabLayout) findViewById(R.id.tab_layout_id);
        mViewPagerId = (ViewPager) findViewById(R.id.viewPager_id);

        setSupportActionBar(mToolbar);
        //getSupportActionBar().setLogo(R.drawable.baby);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new WybierzWiekFragment(), getString(R.string.wybierz_wiek));
        viewPagerAdapter.addFragment(new PoradnikFragment(), getString(R.string.poradnik));

        mViewPagerId.setAdapter(viewPagerAdapter);
        mTabLayoutId.setupWithViewPager(mViewPagerId);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.o_aplikaji) {
            Intent intent = new Intent(this, InfoActivity.class);
            startActivity(intent);
        } else if (id == R.id.email) {
            // TODO: 19.11.17 email
        } else if (id == R.id.nowosci) {
            cl.getLogDialog().show();
        }
        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.manu_item1_id) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
        return true;
    }

}
