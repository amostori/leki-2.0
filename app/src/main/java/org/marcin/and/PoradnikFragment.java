package org.marcin.and;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.marcin.and.adapters.PoradnikClass;
import org.marcin.and.adapters.PoradnikRecyclerAdapter;
import org.marcin.and.poradnik.BigActivity;
import org.marcin.and.poradnik.DefiActivity;
import org.marcin.and.poradnik.GlasgowActivity;
import org.marcin.and.poradnik.KatecholaminyActivity;
import org.marcin.and.poradnik.KedActivity;
import org.marcin.and.poradnik.KonikoActivity;
import org.marcin.and.poradnik.OdmaActivity;
import org.marcin.and.poradnik.PediPacActivity;
import org.marcin.and.poradnik.SkalaApgarActivity;
import org.marcin.and.poradnik.SkalaBoluActivity;
import org.marcin.and.poradnik.StartJumpActivity;
import org.marcin.and.poradnik.WklucieActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by crimson on 12.10.17.
 */

public class PoradnikFragment extends Fragment {

    @Inject
    Bus mBus;
    View mView;
    RecyclerView mRecyclerId;

    List<String> lista = new ArrayList<>();
    DividerItemDecoration mDividerItemDecoration;

    public PoradnikFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.list_wiek_layout, container, false);
        mRecyclerId = mView.findViewById(R.id.recycler_id);
        MyApplication.mComponent.inject(this);
        prepareList();
        Context context = container.getContext();
        LinearLayoutManager manager = new LinearLayoutManager(context);
        mRecyclerId.setLayoutManager(manager);

        mDividerItemDecoration = new DividerItemDecoration(mRecyclerId.getContext(), DividerItemDecoration.VERTICAL);
        mRecyclerId.addItemDecoration(mDividerItemDecoration);
        mRecyclerId.setAdapter(new PoradnikRecyclerAdapter(lista));


        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    private void prepareList() {
        String[] listaPorady = getResources().getStringArray(R.array.porady);
        for (int i = 0; i < listaPorady.length; i++) {
            lista.add(listaPorady[i]);
        }
    }

    @Subscribe
    public void onPoradnikClick(PoradnikClass poradnikClass) {
        int position = poradnikClass.mPosition;
        switch (position) {
            case 0:
                Intent intent = new Intent(getContext(), OdmaActivity.class);
                startActivity(intent);
                break;
            case 1:
                Intent Aintent = new Intent(getContext(), BigActivity.class);
                startActivity(Aintent);
                break;
            case 2:
                Intent Bintent = new Intent(getContext(), DefiActivity.class);
                startActivity(Bintent);
                break;
            case 3:
                Intent Cintent = new Intent(getContext(), PediPacActivity.class);
                startActivity(Cintent);
                break;
            case 4:
                Intent Dintent = new Intent(getContext(), KedActivity.class);
                startActivity(Dintent);
                break;
            case 5:
                Intent Eintent = new Intent(getContext(), KonikoActivity.class);
                startActivity(Eintent);
                break;
            case 6:
                Intent Fintent = new Intent(getContext(), WklucieActivity.class);
                startActivity(Fintent);
                break;
            case 7:
                Intent Gintent = new Intent(getContext(), KatecholaminyActivity.class);
                startActivity(Gintent);
                break;
            case 8:
                Intent Hintent = new Intent(getContext(), StartJumpActivity.class);
                startActivity(Hintent);
                break;
            case 9:
                Intent Iintent = new Intent(getContext(), SkalaBoluActivity.class);
                startActivity(Iintent);
                break;
            case 10:
                Intent Jintent = new Intent(getContext(), SkalaApgarActivity.class);
                startActivity(Jintent);
                break;
            case 11:
                Intent Kintent = new Intent(getContext(), GlasgowActivity.class);
                startActivity(Kintent);
                break;
        }

    }

}
