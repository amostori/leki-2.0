package org.marcin.and.adapters;

import org.marcin.and.DrugListActivity;
import org.marcin.and.PoradnikFragment;
import org.marcin.and.WybierzWiekFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by crimson on 15.10.17.
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    void inject(PoradnikRecyclerAdapter PoradnikRecyclerAdapter);

    void inject(DrugListActivity drugListActivity);

    void inject(WybierzWiekFragment wybierzWiekFragment);

    void inject(PoradnikFragment poradnikFragment);

    void inject(AgeRecyclerManager ageRecyclerManager);


    void inject(LekiListaManager lekiListaManager);

}
