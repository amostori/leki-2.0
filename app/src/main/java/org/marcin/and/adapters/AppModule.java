package org.marcin.and.adapters;

import com.squareup.otto.Bus;

import org.marcin.and.DrugListActivity;
import org.marcin.and.ObliczeniaDawek;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by crimson on 15.10.17.
 */


@Module
public class AppModule {
    @Singleton
    @Provides
    public Bus provideBus() {
        return new Bus();
    }

    @Provides
    public ObliczeniaDawek provideObliczeniaDawek() {
        return new ObliczeniaDawek();
    }
}
