package org.marcin.and.adapters;

/**
 * Created by crimson on 12.10.17.
 */

public class ObiektLeki {
    public String name;
    public String dosage;

    public ObiektLeki() {
    }

    public ObiektLeki(String name, String dosage) {
        this.name = name;
        this.dosage = dosage;
    }

}
