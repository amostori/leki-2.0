package org.marcin.and.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.marcin.and.R;

/**
 * Created by crimson on 12.10.17.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    /*@BindView(R.id.tv_cardview)
    TextView mCardViewTitleTextview;
    @BindView(R.id.myCardView)
    CardView mMyCardView;*/
    TextView mTvItem;
    LinearLayout mLayout;

    public ViewHolder(View itemView) {
        super(itemView);
        mTvItem = itemView.findViewById(R.id.tv_item);
        mLayout = itemView.findViewById(R.id.myConstraintItem);

    }
}
