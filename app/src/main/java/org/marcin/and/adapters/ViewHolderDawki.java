package org.marcin.and.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.marcin.and.R;

/**
 * Created by crimson on 23.10.17.
 */

public class ViewHolderDawki extends RecyclerView.ViewHolder {
    TextView mCardViewTitleTextview;
    TextView mCardViewContentTextView;
    CardView mMyCardView;

    public ViewHolderDawki(View itemView) {
        super(itemView);
        mCardViewTitleTextview = itemView.findViewById(R.id.cardviewTitleTextview);
        mCardViewContentTextView = itemView.findViewById(R.id.cardViewContentTextView);
        mMyCardView = itemView.findViewById(R.id.myCardView);

    }
}
