package org.marcin.and.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;

import org.marcin.and.MyApplication;
import org.marcin.and.R;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by crimson on 12.10.17.
 */

public class PoradnikRecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
    @Inject
    Bus mBus;
    private List<String> lista = new ArrayList<>();

    public PoradnikRecyclerAdapter(List<String> lista) {
        this.lista = lista;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyApplication.mComponent.inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(layoutInflater.inflate(R.layout.recycler_item_no_card, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.mTvItem.setText(lista.get(position));
        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBus.post(new PoradnikClass(position));//sent to PoradnikFragment
            }
        });
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

}
