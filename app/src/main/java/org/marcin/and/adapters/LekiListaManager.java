package org.marcin.and.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;

import org.marcin.and.MyApplication;
import org.marcin.and.R;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by crimson on 23.10.17.
 */

public class LekiListaManager extends RecyclerView.Adapter<ViewHolderDawki> {
    @Inject
    Bus mBus;
    private List<String> mListaTitle;
    private List<String> mListaDawek;
    public static final int ADRENALIN_POSITION = 101;
    public static final int OTHER_POSITION = 102;

    public LekiListaManager(List<String> listaTitles, List<String> listaDawek) {
        mListaTitle = listaTitles;
        mListaDawek = listaDawek;
    }


    @Override
    public ViewHolderDawki onCreateViewHolder(ViewGroup parent, int viewType) {
        MyApplication.mComponent.inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        int layout_id = 0;
        switch (viewType) {
            case ADRENALIN_POSITION:
                layout_id = R.layout.item_recycler_red;
            break;
            case OTHER_POSITION:
                layout_id = R.layout.item_recycler;
                break;
        }
        return new ViewHolderDawki(layoutInflater.inflate(layout_id, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolderDawki holder, final int position) {
        holder.mCardViewTitleTextview.setText(mListaTitle.get(position));
        holder.mCardViewContentTextView.setText(mListaDawek.get(position));
        holder.mMyCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 23.10.17 cardview onclickListener
                mBus.post(new AboutDrugs(position));//OpisLekowActivity
            }
        });

    }

    @Override
    public int getItemCount() {
        return mListaTitle.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 11) {
            return ADRENALIN_POSITION;
        } else {
            return OTHER_POSITION;
        }
    }

}
