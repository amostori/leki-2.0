package org.marcin.and;

import android.app.Application;

import org.marcin.and.adapters.AppComponent;
import org.marcin.and.adapters.AppModule;
import org.marcin.and.adapters.DaggerAppComponent;

/**
 * Created by crimson on 15.10.17.
 */

public class MyApplication extends Application {

    public static AppComponent mComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    /*
    How it works: MainActivity to główna aktywność. Wyświetla NavigationDrawer i main_screen_layout. W layoucie tym jest tab_layout zwaierający TabLayout z ViewPagerem.
    To w ViewPager w MainActivity ładuje się WybierzWiekFragment i PoradnikFragment do TabLoyut używając ViewPagerAdapter.

    WybierzWiekFragment

    //////////
    android:background="?attr/selectableItemBackground"
              android:clickable="true"

              wydaje się nie działać przy ConstraintLayout

    */
}
