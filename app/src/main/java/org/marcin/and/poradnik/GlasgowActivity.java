package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import org.marcin.and.R;

public class GlasgowActivity extends AppCompatActivity {

    Toolbar mToolbar;
    SubsamplingScaleImageView mSubImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glasgow);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSubImage = (SubsamplingScaleImageView) findViewById(R.id.subImage);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSubImage.setImage(ImageSource.resource(R.drawable.glasgow));
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
