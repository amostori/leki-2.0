package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.marcin.and.R;

public class KedActivity extends AppCompatActivity {

    public static final String FILE_ANDROID_ASSET_KEDBUDOWA_JPG = "file:///android_asset/kedbudowa.jpg";
    Toolbar mToolbar;

    ImageView mImageview1;
    ImageView mImageview2;
    ImageView mImageview3;
    ImageView mImageview4;
    ImageView mImageview5;
    ImageView mImageview6;
    ImageView mImageview7;
    ImageView mImageview8;
    ImageView mImageview9;
    ImageView mImageview10;
    ImageView mImageview11;
    ImageView mImageview12;
    ImageView mImageview13;
    ImageView mImageview14;
    ImageView mImageview15;
    ImageView mImageview16;

    ImageView mWvkedsklad1;
    ImageView mWvkedsklad2;
    ImageView mWvkedsklad3;
    ImageView mWvkedsklad4;
    ImageView mWvkedsklad5;
    ImageView mWvkedsklad6;
    ImageView mWvkedsklad7;
    ImageView mWvkedsklad8;
    ImageView mWvkedsklad9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ked);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mImageview1 = (ImageView) findViewById(R.id.imageview1);
        mImageview2 = (ImageView) findViewById(R.id.imageview2);
        mImageview3 = (ImageView) findViewById(R.id.imageview3);
        mImageview4 = (ImageView) findViewById(R.id.imageview4);
        mImageview5 = (ImageView) findViewById(R.id.imageview5);
        mImageview6 = (ImageView) findViewById(R.id.imageview6);
        mImageview7 = (ImageView) findViewById(R.id.imageview7);
        mImageview8 = (ImageView) findViewById(R.id.imageview8);
        mImageview9 = (ImageView) findViewById(R.id.imageview9);
        mImageview10 = (ImageView) findViewById(R.id.imageview10);
        mImageview11 = (ImageView) findViewById(R.id.imageview11);
        mImageview12 = (ImageView) findViewById(R.id.imageview12);
        mImageview13 = (ImageView) findViewById(R.id.imageview13);
        mImageview14 = (ImageView) findViewById(R.id.imageview14);
        mImageview15 = (ImageView) findViewById(R.id.imageview15);
        mImageview16 = (ImageView) findViewById(R.id.imageview16);

        mWvkedsklad1 = (ImageView) findViewById(R.id.wvkedsklad1);
        mWvkedsklad2 = (ImageView) findViewById(R.id.wvkedsklad2);
        mWvkedsklad3 = (ImageView) findViewById(R.id.wvkedsklad3);
        mWvkedsklad4 = (ImageView) findViewById(R.id.wvkedsklad4);
        mWvkedsklad5 = (ImageView) findViewById(R.id.wvkedsklad5);
        mWvkedsklad6 = (ImageView) findViewById(R.id.wvkedsklad6);
        mWvkedsklad7 = (ImageView) findViewById(R.id.wvkedsklad7);
        mWvkedsklad8 = (ImageView) findViewById(R.id.wvkedsklad8);
        mWvkedsklad9 = (ImageView) findViewById(R.id.wvkedsklad9);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setPicasoImages();

    }

    private void setPicasoImages() {
        Picasso.with(getApplicationContext()).load("file:///android_asset/uszy.jpg").into(mImageview1);
        Picasso.with(getApplicationContext()).load("file:///android_asset/kedbudowa.jpg").into(mImageview2);
        Picasso.with(getApplicationContext()).load("file:///android_asset/odchylkeda.jpg").into(mImageview3);
        Picasso.with(getApplicationContext()).load("file:///android_asset/czarne1.jpg").into(mImageview4);
        Picasso.with(getApplicationContext()).load("file:///android_asset/uniesienie.jpg").into(mImageview5);
        Picasso.with(getApplicationContext()).load("file:///android_asset/passrodek.jpg").into(mImageview6);
        Picasso.with(getApplicationContext()).load("file:///android_asset/czarne2.jpg").into(mImageview7);
        Picasso.with(getApplicationContext()).load("file:///android_asset/pupa.jpg").into(mImageview8);
        Picasso.with(getApplicationContext()).load("file:///android_asset/czarne3.jpg").into(mImageview9);
        Picasso.with(getApplicationContext()).load("file:///android_asset/podkladkaked.jpg").into(mImageview10);
        Picasso.with(getApplicationContext()).load("file:///android_asset/zacisk.jpg").into(mImageview11);
        Picasso.with(getApplicationContext()).load("file:///android_asset/obrot.jpg").into(mImageview12);
        Picasso.with(getApplicationContext()).load("file:///android_asset/ciaza1.jpg").into(mImageview13);
        Picasso.with(getApplicationContext()).load("file:///android_asset/ciaza2.jpg").into(mImageview14);
        Picasso.with(getApplicationContext()).load("file:///android_asset/biodro.jpg").into(mImageview15);
        Picasso.with(getApplicationContext()).load("file:///android_asset/miednica.jpg").into(mImageview16);

        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie1.jpg").into(mWvkedsklad1);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie2.jpg").into(mWvkedsklad2);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie3.jpg").into(mWvkedsklad3);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie4.jpg").into(mWvkedsklad4);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie5.jpg").into(mWvkedsklad5);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie6.jpg").into(mWvkedsklad6);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie7.jpg").into(mWvkedsklad7);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie8.jpg").into(mWvkedsklad8);
        Picasso.with(getApplicationContext()).load("file:///android_asset/skladanie9.jpg").into(mWvkedsklad9);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
