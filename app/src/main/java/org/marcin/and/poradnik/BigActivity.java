package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.marcin.and.R;

public class BigActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ImageView mImageview1;
    ImageView mImageview2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_big);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mImageview1 = (ImageView) findViewById(R.id.imageview1);
        mImageview2 = (ImageView) findViewById(R.id.imageview2);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Picasso.with(getApplicationContext()).load("file:///android_asset/kneekid2.jpg").into(mImageview1);
        Picasso.with(getApplicationContext()).load("file:///android_asset/kneeadlt2.jpg").into(mImageview2);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
