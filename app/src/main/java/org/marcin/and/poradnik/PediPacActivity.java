package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.marcin.and.R;

public class PediPacActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ImageView mImageview1;
    ImageView mImageview2;
    ImageView mImageview3;
    ImageView mImageview4;
    ImageView mImageview5;
    ImageView mImageview6;
    ImageView mImageview7;
    ImageView mImageview8;
    ImageView mImageview9;
    ImageView mImageview10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedi_pac);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mImageview1 = (ImageView) findViewById(R.id.imageview1);
        mImageview2 = (ImageView) findViewById(R.id.imageview2);
        mImageview3 = (ImageView) findViewById(R.id.imageview3);
        mImageview4 = (ImageView) findViewById(R.id.imageview4);
        mImageview5 = (ImageView) findViewById(R.id.imageview5);
        mImageview6 = (ImageView) findViewById(R.id.imageview6);
        mImageview7 = (ImageView) findViewById(R.id.imageview7);
        mImageview8 = (ImageView) findViewById(R.id.imageview8);
        mImageview9 = (ImageView) findViewById(R.id.imageview9);
        mImageview10 = (ImageView) findViewById(R.id.imageview10);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Picasso.with(getApplicationContext()).load("file:///android_asset/budowa.jpg").into(mImageview1);
        Picasso.with(getApplicationContext()).load("file:///android_asset/wyciaganie.jpg").into(mImageview2);
        Picasso.with(getApplicationContext()).load("file:///android_asset/ulozenie.jpg").into(mImageview3);
        Picasso.with(getApplicationContext()).load("file:///android_asset/zapiety.jpg").into(mImageview4);
        Picasso.with(getApplicationContext()).load("file:///android_asset/nadgarstek.jpg").into(mImageview5);
        Picasso.with(getApplicationContext()).load("file:///android_asset/nogi1.jpg").into(mImageview6);
        Picasso.with(getApplicationContext()).load("file:///android_asset/nogi2.jpg").into(mImageview7);
        Picasso.with(getApplicationContext()).load("file:///android_asset/ramiona.jpg").into(mImageview8);
        Picasso.with(getApplicationContext()).load("file:///android_asset/glowa.jpg").into(mImageview9);
        Picasso.with(getApplicationContext()).load("file:///android_asset/dodechy.jpg").into(mImageview10);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
