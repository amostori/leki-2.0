package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

import org.marcin.and.R;

public class StartJumpActivity extends AppCompatActivity {

    Toolbar mToolbar;
    SubsamplingScaleImageView mScaleIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_jump);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mScaleIv = (SubsamplingScaleImageView) findViewById(R.id.scaleIv);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mScaleIv.setImage(ImageSource.resource(R.drawable.jump));
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
