package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.marcin.and.R;

public class OdmaActivity extends AppCompatActivity {

    public static final String FILE_ANDROID_ASSET_PNXBIG_JPG = "file:///android_asset/pnxbig.jpg";
    Toolbar mToolbar;
    ImageView mIv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_odma);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mIv1 = (ImageView) findViewById(R.id.iv1);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Picasso.with(getApplicationContext()).load(FILE_ANDROID_ASSET_PNXBIG_JPG).into(mIv1);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
