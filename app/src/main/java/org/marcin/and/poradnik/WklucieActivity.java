package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import org.marcin.and.R;

public class WklucieActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ImageView mIv1;
    ImageView mIv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wklucie);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mIv1 = (ImageView) findViewById(R.id.iv1);
        mIv2 = (ImageView) findViewById(R.id.iv2);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Picasso.with(getApplicationContext()).load("file:///android_asset/zyla1.jpg").into(mIv1);
        Picasso.with(getApplicationContext()).load("file:///android_asset/zyla2.jpg").into(mIv2);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
