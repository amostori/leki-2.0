package org.marcin.and.poradnik;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.marcin.and.DrugListActivity;
import org.marcin.and.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WagaActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.textView)
    TextView mTextView;
    @BindView(R.id.imageView2)
    ImageView mImageView2;
    @BindView(R.id.imageView)
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waga);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String stringExtra = getIntent().getStringExtra(DrugListActivity.DRUG_LIST);
        setTexts(stringExtra);
    }

    private void setTexts(String stringExtra) {
        switch (stringExtra) {
            case "WAGA":
                getSupportActionBar().setTitle(R.string.waga);
                mTextView.setText(R.string.waga_info);
                mImageView.setVisibility(View.INVISIBLE);
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "RR":
                getSupportActionBar().setTitle(R.string.rr);
                mTextView.setText(R.string.rr_info);
                mImageView.setVisibility(View.INVISIBLE);
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "ODDECH":
                getSupportActionBar().setTitle(R.string.oddech);
                mTextView.setText(R.string.oddech_info);
                mImageView.setVisibility(View.INVISIBLE);
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "TETNO":
                getSupportActionBar().setTitle(R.string.tetno);
                mTextView.setText(R.string.tetno_info);
                mImageView.setVisibility(View.INVISIBLE);
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "GLUKOZA":
                getSupportActionBar().setTitle(R.string.glukoza);
                mTextView.setText(R.string.glukoza_info);
                mImageView.setVisibility(View.INVISIBLE);
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "INTUBACJA":
                getSupportActionBar().setTitle(R.string.intubacja);
                mTextView.setText(R.string.intubacja_info);
                mImageView.setVisibility(View.INVISIBLE);
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "LARYN":
                getSupportActionBar().setTitle(R.string.laryngoskop);
                mTextView.setText(R.string.laryngoskop_info);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageDrawable(getResources().getDrawable(R.drawable.laryng));
                mImageView2.setVisibility(View.VISIBLE);
                mImageView2.setImageDrawable(getResources().getDrawable(R.drawable.laryng2));
                break;
            case "MASKA":
                getSupportActionBar().setTitle(R.string.maska);
                mTextView.setText(R.string.maska_info);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageDrawable(getResources().getDrawable(R.drawable.maska));
                mImageView2.setVisibility(View.INVISIBLE);
                break;
            case "DEFI":
                getSupportActionBar().setTitle(R.string.defi);
                mTextView.setText(R.string.defi_info);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageDrawable(getResources().getDrawable(R.drawable.defi1));
                mImageView2.setVisibility(View.VISIBLE);
                mImageView2.setImageDrawable(getResources().getDrawable(R.drawable.pads));
                break;
            case "KARDIO":
                getSupportActionBar().setTitle(R.string.kardio);
                mTextView.setText(R.string.kardio_info);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageDrawable(getResources().getDrawable(R.drawable.pads));
                mImageView2.setVisibility(View.INVISIBLE);
                break;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
